<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m200106_092948_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'category_name' => $this->string(255)->notNull(),
            'status' => "ENUM('active','inactive')"
        ]);
        // creates index for column `category_id`
        $this->createIndex(
            'idx-category-category_id',
            'post',
            'category_id'
        );

        // add foreign key for table `category`
        $this->addForeignKey(
            'fk-category-category_id',
            'post',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-category-category_id',
            'post'
        );
        $this->dropIndex(
            'idx-category-category_id',
            'post'
        );
        $this->dropTable('{{%category}}');
    }
}
