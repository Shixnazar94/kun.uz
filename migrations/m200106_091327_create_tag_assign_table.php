<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tag_assign}}`.
 */
class m200106_091327_create_tag_assign_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tag_assign}}', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `tag_id`
        $this->createIndex(
            'idx-tag_assign-tag_id',
            'tag_assign',
            'tag_id'
        );

        // add foreign key for table `tag`
        $this->addForeignKey(
            'fk-tag_assign-tag_id',
            'tag_assign',
            'tag_id',
            'tag',
            'id',
            'CASCADE'
        );

        // create index for column 'post_id'

        $this->createIndex(
          'idx-tag_assign-post_id',
          'tag_assign',
          'post_id'
        );

        // create foreign key for table 'post'
        $this->addForeignKey(
            'fk-tag_assign-post_id',
            'tag_assign',
            'post_id',
            'post',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tag_assign}}');
    }
}
