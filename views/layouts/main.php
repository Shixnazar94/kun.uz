<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="animsition">
<?php $this->beginBody() ?>
<?php
    $allCategories = \app\models\Category::find()->where(['status' => 'active'])->all();
    $popularPost = \app\models\Post::find()->orderBy(['count_view' => SORT_DESC])->all();
?>
<!-- Header -->
<header>
    <!-- Header desktop -->
    <div class="container-menu-desktop">
        <div class="topbar">
            <div class="content-topbar container h-100">
                <div class="left-topbar">
						<span class="left-topbar-item flex-wr-s-c">
							<span>
								Uzbekiston
							</span>

							<img class="m-b-1 m-rl-8" src="/images/icons/icon-night.png" alt="IMG">

							<span>
								HI 58° LO 56°
							</span>
						</span>
                    <?= Html::a('Biz haqimizda',['about'],['class' => 'left-topbar-item'])?>
                    <?= Html::a('Murojat',['contact'],['class' => 'left-topbar-item'])?>
                    <?= Html::a('Ro\'yxatdan o\'tish' ,['signup'],['class' => 'left-topbar-item'])?>
                    <?php if(Yii::$app->user->isGuest):?>
                        <?= Html::a('Kirish' ,['login'],['class' => 'left-topbar-item'])?>
                    <?php endif;?>
                    <?php if(!Yii::$app->user->isGuest):?>
                        <?= Html::a('Chiqish' ,['logout'],['class' => 'left-topbar-item', 'data' => ['method' => 'post']])?>
                    <?php endif;?>
                </div>

                <div class="right-topbar">
                    <a href="http:\\facebook.com">
                        <span class="fab fa-facebook-f"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-twitter"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-pinterest-p"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-vimeo-v"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-youtube"></span>
                    </a>
                </div>
            </div>
        </div>

        <!-- Header Mobile -->
        <div class="wrap-header-mobile">
            <!-- Logo moblie -->
            <div class="logo-mobile">
                <h style="font-size: 40px"><a href="/">KUN.uz</a></h>
            </div>

            <!-- Button show menu -->
            <div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
            </div>
        </div>

        <!-- Menu Mobile -->
        <div class="menu-mobile">
            <ul class="topbar-mobile">
                <li class="left-topbar">
						<span class="left-topbar-item flex-wr-s-c">
							<span>
								Uzbekiston
							</span>

							<img class="m-b-1 m-rl-8" src="/images/icons/icon-night.png" alt="IMG">

							<span>
								HI 58° LO 56°
							</span>
						</span>
                </li>

                <li class="left-topbar">
                    <?= Html::a('Biz haqimizda',['about'],['class' => 'left-topbar-item'])?>
                    <?= Html::a('Murojat',['contact'],['class' => 'left-topbar-item'])?>
                    <?= Html::a('Ro\'yxatdan o\'tish' ,['signup'],['class' => 'left-topbar-item'])?>
                    <?php if(Yii::$app->user->isGuest):?>
                        <?= Html::a('Kirish' ,['login'],['class' => 'left-topbar-item'])?>
                    <?php endif;?>
                    <?php if(!Yii::$app->user->isGuest):?>
                        <?= Html::a('Chiqish' ,['logout'],['class' => 'left-topbar-item', 'data' => ['method' => 'post']])?>
                    <?php endif;?>
                </li>

                <li class="right-topbar">
                    <a href="#">
                        <span class="fab fa-facebook-f"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-twitter"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-pinterest-p"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-vimeo-v"></span>
                    </a>

                    <a href="#">
                        <span class="fab fa-youtube"></span>
                    </a>
                </li>
            </ul>

            <ul class="main-menu-m">
                <?php foreach ($allCategories as $one): ?>
                <li>
                    <?= Html::a($one->category_name,['category', 'id' => $one->id])?>
                </li>
                <?php endforeach;?>
            </ul>
        </div>

        <!--  -->
        <div class="wrap-logo container">
            <!-- Logo desktop -->
            <div class="logo">
                <h1 style="font-size: 40px"><a href="/">KUN.uz</a></h1>
            </div>
        </div>

        <!--  -->
        <div class="wrap-main-nav">
            <div class="main-nav">
                <!-- Menu desktop -->
                <nav class="menu-desktop">
                    <a class="logo-stick" href="index.html">
                        <img src="/images/icons/logo-01.png" alt="LOGO">
                    </a>

                    <ul class="main-menu">
                        <li>
                            <?= Html::a('Bosh sahifa',['/'])?>
                        </li>
                        <?php foreach ($allCategories as $category):?>
                        <li class="mega-menu-item">
                            <?= Html::a($category->category_name,['site/category', 'id' => $category->id])?>
<!--                            <a href="category-01.html">--><?//= $category->category_name?><!--</a>-->
                        </li>
                        <?php endforeach;?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<!-- Headline -->
<div class="container">
    <div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8 pull-right">
        <div class="pos-relative size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6 pull-right">
            <input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="text" name="search" placeholder="Qidirish">
            <button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
                <i class="zmdi zmdi-search"></i>
            </button>
        </div>
    </div>
</div>
<div class="container">
    <?=$content?>
</div>
<!-- Footer -->
<footer>
    <div class="bg2 p-t-40 p-b-25">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 p-b-20">
                    <div class="size-h-3 flex-s-c">
                           <h1 style="font-size: 40px;"><a href="/">KUN.UZ</a></h1>
                    </div>

                    <div>
                        <p class="f1-s-1 cl11 p-b-16">
                            XXI asr – axborot asri. Unda odamlar axborot oqimida yashaydi. Shuni inobatga olib, biz loyiha nomini “Kun” deb nomladik.

                            “Kun” – bu jahonda va O‘zbekistonda sodir bo‘layotgan eng so‘nggi yangiliklarni o‘zbek o‘quvchilariga sodda va tushunarli tilda hamda tezkor yetkazib berishga mo‘ljallangan internet-nashrdir.

                            “Kun” — O‘zbekistondagi eng ommabop internet-nashr bo‘lib, unga har kuni 300 000 kishi tashrif buyuradi.
                        <p class="f1-s-1 cl11 p-b-16">
                            Savollar uchun? Telefon nomer (+97) 777 77 77
                        </p>

                        <div class="p-t-15">
                            <a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                                <span class="fab fa-facebook-f"></span>
                            </a>

                            <a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                                <span class="fab fa-twitter"></span>
                            </a>

                            <a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                                <span class="fab fa-pinterest-p"></span>
                            </a>

                            <a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                                <span class="fab fa-vimeo-v"></span>
                            </a>

                            <a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                                <span class="fab fa-youtube"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4 p-b-20">
                    <div class="size-h-3 flex-s-c">
                        <h5 class="f1-m-7 cl0">
                            Ko'p o'qilganlar
                        </h5>
                    </div>

                    <ul>
                        <?php $cnt=1;?>
                        <?php foreach ($popularPost as $p): ?>
                        <?php if ($cnt == 4) break; ?>
                            <li class="flex-wr-sb-s p-b-20">
                            <a href="<?= Url::to(['site/post', 'id' => $p->id])?>" class="size-w-4 wrap-pic-w hov1 trans-03">
                                <img src="/<?= $p->image?>" alt="IMG">
                            </a>
                            <div class="size-w-5">
                                <h6 class="p-b-5">
                                    <?= Html::a($p->title,['site/post', 'id' => $p->id],['class' => "f1-s-5 cl11 hov-cl10 trans-03"])?>
                                </h6>
                            </div>
                        </li>
                        <?php $cnt++;?>
                        <?php endforeach;?>
                    </ul>
                </div>

                <div class="col-sm-6 col-lg-4 p-b-20">
                    <div class="size-h-3 flex-s-c">
                        <h5 class="f1-m-7 cl0">
                            Menyular
                        </h5>
                    </div>

                    <ul class="m-t--12">
                        <?php foreach ($allCategories as $one): ?>
                            <li class="how-bor1 p-rl-5 p-tb-10">
                                <?= Html::a($one->category_name,['category', 'id' => $one->id],['class' => 'f1-s-5 cl11 hov-cl10 trans-03 p-tb-8'])?>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="bg11">
        <div class="container size-h-4 flex-c-c p-tb-15">
				<span class="f1-s-1 cl0 txt-center">
					Copyright © 2020
					Hizmatlar litsenziyalangan
				</span>
        </div>
    </div>
</footer>

<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<span class="fas fa-angle-up"></span>
		</span>
</div>

<!-- Modal Video 01-->
<div class="modal fade" id="modal-video-01" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" data-dismiss="modal">
        <div class="close-mo-video-01 trans-0-4" data-dismiss="modal" aria-label="Close">&times;</div>

        <div class="wrap-video-mo-01">
            <div class="video-mo-01">
                <iframe src="https://www.youtube.com/embed/wJnBTPUQS5A?rel=0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
