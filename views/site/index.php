<?php

/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Html;
$this->title = 'Bosh sahifa';
?>
<!-- Post -->
<section class="bg0 p-t-70 p-b-55">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 p-b-80">
                <div class="row">
                    <?php foreach ($posts as $post):?>
                    <div class="col-sm-6 p-r-25 p-r-15-sr991">
                        <!-- Item latest -->
                        <div class="m-b-45">
                            <a href="/site/post?id=<?= $post->id?>" class="wrap-pic-w hov1 trans-03">
                                <img src="/<?= $post->image?>" alt="IMG" height="300">
                            </a>
<!--                            --><?//= \yii\helpers\Html::a('<img src="$post->image" alt="IMG" height="300">',['post'],['class' => 'wrap-pic-w hov1 trans-03'])?>
                            <div class="p-t-16">
                                <h5 class="p-b-5">
                                    <a href="/site/post?id=<?= $post->id?>" class="f1-m-3 cl2 hov-cl10 trans-03">
                                        <?= $post->title?>
                                    </a>
                                </h5>
                                <h6>Ko'rilgan: <?= $post->count_view?></h6>
                                <span class="cl8">
										<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
											avtor <?=$post->user->fullname?>
										</a>

										<span class="f1-s-3 m-rl-3">
											-
										</span>

										<span class="f1-s-3">
											<?=$post->created_at?>
										</span>
									</span>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>

                <!-- Pagination -->
                <div class="flex-wr-s-c m-rl--7 p-t-15">
                    <?= LinkPager::widget([
                        'pagination' => $pagination,
                    ]);?>
                </div>
            </div>

            <div class="col-md-10 col-lg-4 p-b-80">
                <div class="p-l-10 p-rl-0-sr991">
                    <!-- Most Popular -->
                    <div class="p-b-23">
                        <div class="how2 how2-cl4 flex-s-c">
                            <h3 class="f1-m-2 cl3 tab01-title">
                                Eng ko'p o'qilganlar
                            </h3>
                        </div>

                        <ul class="p-t-35">
                            <?php $id=1;?>
                            <?php foreach ($popularPosts as $one): ?>
                            <li class="flex-wr-sb-s p-b-22">
                                <div class="size-a-8 flex-c-c borad-3 size-a-8 bg9 f1-m-4 cl0 m-b-6">
                                    <?= $id++?>
                                </div>
                                <?= \yii\helpers\Html::a($one->title,['post', 'id' => $one->id],['class' => 'size-w-3 f1-s-7 cl3 hov-cl10 trans-03'])?>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>

                    <!-- Tag -->
                    <div>
                        <div class="how2 how2-cl4 flex-s-c m-b-30">
                            <h3 class="f1-m-2 cl3 tab01-title">
                                Teglar
                            </h3>
                        </div>

                        <div class="flex-wr-s-s m-rl--5">
                            <?php foreach ($tags as $tag): ?>
                            <?= Html::a($tag->name,['site/tag', 'id' => $tag->id],['class' => 'flex-c-c size-h-2 bo-1-rad-20 bocl12 f1-s-1 cl8 hov-btn2 trans-03 p-rl-20 p-tb-5 m-all-5'])?>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Latest -->
<section class="bg0 p-t-60 p-b-35">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 p-b-20">
                <div class="how2 how2-cl4 flex-s-c m-r-10 m-r-0-sr991">
                    <h3 class="f1-m-2 cl3 tab01-title">
                        So'ngi habarlar
                    </h3>
                </div>

                <div class="row p-t-35">
                    <?php foreach ($newPosts as $post): ?>
                    <div class="col-sm-6 p-r-25 p-r-15-sr991">
                        <!-- Item latest -->
                        <div class="m-b-45">
                            <a href="site/post?id=<?= $post->id?>" class="wrap-pic-w hov1 trans-03">
                                <img src="/<?=$post->image?>" alt="IMG" width="200" height="250">
                            </a>

                            <div class="p-t-16">
                                <h5 class="p-b-5">
                                    <a href="site/post?id=<?= $post->id?>" class="f1-m-3 cl2 hov-cl10 trans-03">
                                        <?= $post->title?>
                                        <p class="text-success">Ko'rilganlar: <?= $post->count_view?></p>
                                    </a>
                                </h5>

                                <span class="cl8">
										<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
											avtor <?= $post->user->fullname?>
										</a>

										<span class="f1-s-3 m-rl-3">
											-
										</span>

										<span class="f1-s-3">
											<?= $post->created_at?>
										</span>
									</span>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
