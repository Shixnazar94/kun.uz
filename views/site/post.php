<?php
use yii\helpers\Html;
?>
<!-- Content -->
<section class="bg0 p-b-70 p-t-5">
    <!-- Title -->
    <div class="bg-img1 size-a-18 how-overlay1" style="background-image: url('/<?=$post->image?>');">
        <div class="container h-full flex-col-e-c p-b-58">
            <?= Html::a($post->title, ['site/post', 'id' => $post->id],['class' => 'f1-s-10 cl0 hov-cl10 trans-03 text-uppercase txt-center m-b-33'])?>
            <h3 class="f1-l-5 cl0 p-b-16 txt-center respon2">
                <?= $post->description?>
            </h3>

            <div class="flex-wr-c-s">
					<span class="f1-s-3 cl8 m-rl-7 txt-center">
						<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
							avtor <?= $post->user->fullname?>
						</a>

						<span class="m-rl-3">-</span>

						<span>
							<?= $post->created_at?>
						</span>
					</span>

                <span class="f1-s-3 cl8 m-rl-7 txt-center">
						<?= $post->count_view ?> Views
					</span>
            </div>
        </div>
    </div>

    <!-- Detail -->
    <div class="container p-t-82">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 p-b-100">
                <div class="p-r-10 p-r-0-sr991">
                    <!-- Blog Detail -->
                    <div class="p-b-70">
                        <p class="f1-s-11 cl6 p-b-25">
                            <?= $post->content?>
                        </p>
                        <!-- Tag -->
                        <div class="flex-s-s p-t-12 p-b-15">
								<span class="f1-s-12 cl5 m-r-8">
									Teglar:
								</span>

                            <div class="flex-wr-s-s size-w-0">
                                <?php foreach ($tags as $tag):?>
                                <a href="#" class="f1-s-12 cl8 hov-link1 m-r-15">
                                   <?= $tag->tag->name?>
                                </a>
                                <?php endforeach;?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
