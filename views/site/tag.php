<!-- Post -->
<section class="bg0 p-t-70 p-b-55">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 p-b-80">
                <div class="row">
                    <?php foreach ($posts as $post):?>
                        <div class="col-sm-6 p-r-25 p-r-15-sr991">
                            <!-- Item latest -->
                            <div class="m-b-45">
                                <a href="/site/post?id=<?= $post['id']?>" class="wrap-pic-w hov1 trans-03">
                                    <img src="/<?= $post['image']?>" alt="IMG">
                                </a>

                                <div class="p-t-16">
                                    <h5 class="p-b-5">
                                        <a href="/site/post?id=<?= $post['id']?>" class="f1-m-3 cl2 hov-cl10 trans-03">
                                            <?= $post['title']?>
                                        </a>
                                    </h5>

                                    <span class="cl8">
										<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
											avtor <?= $post['user']['fullname']?>
										</a>

										<span class="f1-s-3 m-rl-3">
											-
										</span>

										<span class="f1-s-3">
											<?= $post['created_at']?>
										</span>
									</span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
