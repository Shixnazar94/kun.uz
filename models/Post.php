<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property int $user_id
 * @property int $category_id
 * @property string $description
 * @property string $content
 * @property int|null $count_view
 * @property string|null $status
 * @property string|null $created_at
 *
 * @property Category $category
 * @property User $user
 * @property TagAssign[] $tagAssigns
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */

   // public $image;
    public $file;
    public function rules()
    {
        return [
            [['title', 'user_id', 'category_id', 'description', 'content'], 'required'],
            [['user_id', 'category_id', 'count_view'], 'integer'],
            [['content', 'status'], 'string'],
            [['created_at'], 'safe'],
            [['file'], 'file', 'extensions' => 'jpg,png'],
            [['title', 'description','image'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'user_id' => 'User',
            'category_id' => 'Category',
            'description' => 'Description',
            'content' => 'Content',
            'count_view' => 'Count View',
            'status' => 'Status',
            'created_at' => 'Created At',
            'file' => 'File',
            'image' => 'Image'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagAssigns()
    {
        return $this->hasMany(TagAssign::className(), ['post_id' => 'id']);
    }
}
