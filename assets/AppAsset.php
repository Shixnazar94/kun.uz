<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'fonts/fontawesome-5.0.8/css/fontawesome-all.min.css',
        'fonts/iconic/css/material-design-iconic-font.min.css',
        'css/animate.css',
        'css/hamburgers.min.css',
        'css/animsition.min.css',
        'css/util.min.css',
        'css/main.css',
    ];
    public $js = [
        'js/jquery-3.2.1.min.js',
        'js/animsition.min.js',
        'js/popper.js',
        'js/bootstrap.min.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
