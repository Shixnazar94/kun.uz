<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/admin/font-awesome/css/font-awesome.css',
        'css/admin/jquery.gritter.css',
        'css/admin/zabuto_calendar.css',
        'css/admin/style.css',
        'css/admin/style_m.css',
        'css/admin/style-responsive.css',
    ];
    public $js = [
        'js/admin/jquery.js',
        'js/admin/jquery-1.8.3.min.js',
        'js/admin/bootstrap.min.js',
        'js/admin/jquery.dcjqaccordion.2.7.js',
        'js/admin/jquery.scrollTo.min.js',
        'js/admin/jquery.nicescroll.js',
        'js/admin/jquery.sparkline.js',
        'js/admin/common-scripts.js',
        'js/admin/jquery.gritter.js',
        'js/admin/gritter-conf.js',
        'js/admin/sparkline-chart.js',
        'js/admin/zabuto_calendar.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
