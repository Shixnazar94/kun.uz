<?php

namespace app\modules\admin\controllers;

use PHPUnit\Framework\Constraint\ExceptionMessageTest;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex()
    {
        $this->layout="adminka";
        if(\Yii::$app->user->isGuest || \Yii::$app->user->identity->role != 'admin')
            throw new NotFoundHttpException('Sahifa topilmadi');
        return $this->render('index');
    }
}
