<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

        //    'id',
            'title',
            [
                    'attribute' => 'user_id',
                    'value' => 'user.fullname'
            ],
            [
                'attribute' => 'category_id',
                'value' => 'category.category_name'
            ],
            'description',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Image',
                'format' =>'raw',
                'value' => function($data){
                    return '<img width="150" class="img-rounded" height="80" src="/'.$data->image.'"/>';
                }
            ],
            //'content:ntext',
            //'count_view',
            //'status',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
