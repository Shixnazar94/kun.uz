<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<section id="container" >
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
<!--        <a href="#" class="logo"><b>ADMIN PANEL</b></a>-->
        <?= Html::a('<b>ADMIN PANEL</b>',['index'],['class' => 'logo'])?>
        <div class="top-menu">
            <ul class="nav pull-right top-menu">
                <li><?= Html::a('Logout',['/site/logout'],['class' => 'logout','data' => ['method' => 'post']])?></li>
            </ul>
        </div>
    </header>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <p class="centered"><a href="profile.html"><img src="/<?=Yii::$app->user->identity->image?>" class="img-circle" width="80"></a></p>
                <h5 class="centered"><?=Yii::$app->user->identity->fullname?></h5>
                <li class="mt">
                    <?= Html::a('
                        <i class="fa fa-dashboard"></i>
                        <span>Category</span>
                    ',['category/'])?>
                </li>
                <li class="mt">
                    <?= Html::a('
                        <i class="fa fa-print"></i>
                        <span>Post</span>
                    ',['post/'])?>
                </li>
                <li class="mt">
                    <?= Html::a('
                        <i class="fa fa-tag"></i>
                        <span>Tag</span>
                    ',['tag/'])?>
                </li>
                <li class="mt">
                    <?= Html::a('
                        <i class="fa fa-anchor"></i>
                        <span>Tag Assign</span>
                    ',['tag-assign/'])?>
                </li>
                <li class="mt">
                    <?= Html::a('
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                    ',['users/'])?>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <div class="container">
        <div class="row">
            <br><br><br><br><br>
            <div class="col-md-offset-2">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content?>
            </div>
        </div>
    </div>
    <!--main content end-->
</section>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
