<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TagAssignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tag Assigns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-assign-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tag Assign', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'id',
           // 'tag_id',
            [
                    'attribute' => 'tag_id',
                    'value' => 'tag.name'
            ],
          //  'post_id',
            [
                    'attribute' => 'post_id',
                    'value' => 'post.title'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
