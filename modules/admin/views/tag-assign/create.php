<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TagAssign */

$this->title = 'Create Tag Assign';
$this->params['breadcrumbs'][] = ['label' => 'Tag Assigns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-assign-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
