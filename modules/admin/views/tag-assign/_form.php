<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Tag;
use app\models\Post;
/* @var $this yii\web\View */
/* @var $model app\models\TagAssign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-assign-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'tag_id')->dropDownList(
                    ArrayHelper::map(Tag::find()->all(),'id','name')
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'post_id')->dropDownList(
                    ArrayHelper::map(Post::find()->all(),'id','title')
            ) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
