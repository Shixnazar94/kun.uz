<?php

namespace app\controllers;

use app\models\Post;
use app\models\Tag;
use app\models\TagAssign;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use yii\web\UploadedFile;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $tags = Tag::find()->all();
        $query = Post::find()->where(['status' => 'active']);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count]);
        $posts = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $popularPosts = Post::find()->orderBy(['count_view' => SORT_DESC])->all();
        $newPosts = Post::find()->orderBy(['created_at' => SORT_DESC])->all();
        return $this->render('index',['posts' => $posts, 'tags' => $tags, 'popularPosts' => $popularPosts,'newPosts' => $newPosts, 'pagination' => $pagination]);
    }

    public function actionSignup()
    {
        if(!Yii::$app->user->isGuest)
            return $this->goHome();

        $model = new Users();

        if($model->load(Yii::$app->request->post())){
            $model->setPassword($model->password);
            $model->generateAuthKey();
            $model->status = 'active';
            $model->role = 'user';
            $model->created_at = date('Y-m-d H:m:s');
            $model->file = UploadedFile::getInstance($model,'file');
            if(!empty($model->file))
            {
                $imageName = $model->fullname;
                $model->file->saveAs('uploads/users/'.$imageName.'.'.$model->file->extension);
                $model->image = 'uploads/users/'.$imageName.'.'.$model->file->extension;
//                echo '<pre>';
//                print_r($model);
//                die();
            }
            if($model->validate())
            {
                $model->save();
                Yii::$app->session->setFlash('success', "You have signed up successfully. Pleace login.");
                return $this->redirect(['site/login']);
            }
        }
        $model->password='';
        return $this->render('signup',[
            'model'=>$model,
        ]);
    }
    public function actionTag($id)
    {
        $assigns = TagAssign::find()->where(['tag_id' => $id])->all();
        $posts = [];
        foreach ($assigns as $assign)
        {
            $post = Post::findOne($assign->post_id);
            $posts[]= $post;
        }
//        echo '<pre>';
//        print_r($posts);
//        die();
        return $this->render('tag',['posts' => $posts]);
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }


        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    public function actionCategory($id)
    {
        $posts = Post::find()->where(['category_id' => $id])->all();
        return $this->render('category',['posts' => $posts]);
    }
    public function actionPost($id)
    {
        $tag_assaign = TagAssign::find()->where(['post_id' => $id])->all();
        $post = Post::findOne($id);
        $post->count_view++;
        $post->save();
        return $this->render('post',['post' => $post, 'tags' => $tag_assaign]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
